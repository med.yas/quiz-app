package ml.medyas.isetquizapp;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import static ml.medyas.isetquizapp.LoginActivity.EMAIL_KEY;

public class MainActivity extends AppCompatActivity {

    public static Boolean isConnected = false;

    private RadioButton radioButtonQ11, radioButtonQ12,radioButtonQ13,radioButtonQ14,radioButtonQ21,radioButtonQ22,radioButtonQ23,
            radioButtonQ24,radioButtonQ31,radioButtonQ32,radioButtonQ33,radioButtonQ34,radioButtonQ41,radioButtonQ42,radioButtonQ43, radioButtonQ44,
            radioButtonQ51,radioButtonQ52,radioButtonQ53,radioButtonQ54,radioButtonQ61,radioButtonQ62,radioButtonQ63,radioButtonQ64;

    private Button buttonCheckQuiz, buttonReplay;
    private RadioGroup radioGroupQ1, radioGroupQ2, radioGroupQ3, radioGroupQ4, radioGroupQ5, radioGroupQ6;
    private int[] quizAnswers = {R.id.radioButton_q1_1, R.id.radioButton_q2_1, R.id.radioButton_q3_4, R.id.radioButton_q4_1, R.id.radioButton_q5_1, R.id.radioButton_q6_1};
    private int quizScore = 0;

    private TextView emailAddress;

    private String email = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(getIntent() != null ){
            email = (String) getIntent().getExtras().get(EMAIL_KEY);
        }

        getSupportActionBar().setTitle("Quiz App");

        emailAddress = (TextView) findViewById(R.id.email_address);
        emailAddress.setText(email);
        emailAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ListActivity.class));
            }
        });

        radioButtonQ11 = (RadioButton) findViewById(R.id.radioButton_q1_1);
        radioButtonQ12 = (RadioButton) findViewById(R.id.radioButton_q1_2);
        radioButtonQ13 = (RadioButton) findViewById(R.id.radioButton_q1_3);
        radioButtonQ14 = (RadioButton) findViewById(R.id.radioButton_q1_4);
        radioButtonQ21 = (RadioButton) findViewById(R.id.radioButton_q2_1);
        radioButtonQ22 = (RadioButton) findViewById(R.id.radioButton_q2_2);
        radioButtonQ23 = (RadioButton) findViewById(R.id.radioButton_q2_3);
        radioButtonQ24 = (RadioButton) findViewById(R.id.radioButton_q2_4);
        radioButtonQ31 = (RadioButton) findViewById(R.id.radioButton_q3_1);
        radioButtonQ32 = (RadioButton) findViewById(R.id.radioButton_q3_2);
        radioButtonQ33 = (RadioButton) findViewById(R.id.radioButton_q3_3);
        radioButtonQ34 = (RadioButton) findViewById(R.id.radioButton_q3_4);
        radioButtonQ41 = (RadioButton) findViewById(R.id.radioButton_q4_1);
        radioButtonQ42 = (RadioButton) findViewById(R.id.radioButton_q4_2);
        radioButtonQ43 = (RadioButton) findViewById(R.id.radioButton_q4_3);
        radioButtonQ44 = (RadioButton) findViewById(R.id.radioButton_q4_4);
        radioButtonQ51 = (RadioButton) findViewById(R.id.radioButton_q5_1);
        radioButtonQ52 = (RadioButton) findViewById(R.id.radioButton_q5_2);
        radioButtonQ53 = (RadioButton) findViewById(R.id.radioButton_q5_3);
        radioButtonQ54 = (RadioButton) findViewById(R.id.radioButton_q5_4);
        radioButtonQ61 = (RadioButton) findViewById(R.id.radioButton_q6_1);
        radioButtonQ62 = (RadioButton) findViewById(R.id.radioButton_q6_2);
        radioButtonQ63 = (RadioButton) findViewById(R.id.radioButton_q6_3);
        radioButtonQ64 = (RadioButton) findViewById(R.id.radioButton_q6_4);

        radioGroupQ1 = (RadioGroup) findViewById(R.id.radioGroup_q1);
        radioGroupQ2 = (RadioGroup) findViewById(R.id.radioGroup_q2);
        radioGroupQ3 = (RadioGroup) findViewById(R.id.radioGroup_q3);
        radioGroupQ4 = (RadioGroup) findViewById(R.id.radioGroup_q4);
        radioGroupQ5 = (RadioGroup) findViewById(R.id.radioGroup_q5);
        radioGroupQ6 = (RadioGroup) findViewById(R.id.radioGroup_q6);


        buttonCheckQuiz = (Button) findViewById(R.id.button_check_quiz);
        buttonReplay = (Button) findViewById(R.id.button_restart_quiz);

        buttonReplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioGroupQ1.clearCheck();
                radioGroupQ2.clearCheck();
                radioGroupQ3.clearCheck();
                radioGroupQ4.clearCheck();
                radioGroupQ5.clearCheck();
                radioGroupQ6.clearCheck();
                buttonCheckQuiz.setVisibility(View.VISIBLE);
                buttonReplay.setVisibility(View.GONE);
            }
        });

        buttonCheckQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (radioGroupQ1.getCheckedRadioButtonId() == quizAnswers[0]) {
                    quizScore++;
                }
                if (radioGroupQ2.getCheckedRadioButtonId() == quizAnswers[1]) {
                    quizScore++;
                }
                if (radioGroupQ3.getCheckedRadioButtonId() == quizAnswers[2]) {
                    quizScore++;
                }
                if (radioGroupQ4.getCheckedRadioButtonId() == quizAnswers[3]) {
                    quizScore++;
                }
                if (radioGroupQ5.getCheckedRadioButtonId() == quizAnswers[4]) {
                    quizScore++;
                }
                if (radioGroupQ6.getCheckedRadioButtonId() == quizAnswers[5]) {
                    quizScore++;
                }

                buttonCheckQuiz.setVisibility(View.GONE);
                buttonReplay.setVisibility(View.VISIBLE);

                showResultDialog();
            }
        });
    }

    private void showResultDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_layout, null);
        dialogBuilder.setView(dialogView);

        TextView result = (TextView) dialogView.findViewById(R.id.result_text);
        ImageView image = (ImageView) dialogView.findViewById(R.id.result_image);
        if(quizScore >=4) {
            image.setImageResource(R.drawable.ic_tag_faces_black_24dp);
        } else if(quizScore == 3) {
            image.setImageResource(R.drawable.ic_sentiment_neutral_black_24dp);
        } else {
            image.setImageResource(R.drawable.ic_sentiment_dissatisfied_black_24dp);
        }

        result.setText("Your Score is "+quizScore);

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }
}
