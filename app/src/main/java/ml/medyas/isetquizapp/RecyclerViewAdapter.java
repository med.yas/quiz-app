package ml.medyas.isetquizapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.viewHolder> {
    private String names[];
    private Context context;

    public class viewHolder extends RecyclerView.ViewHolder {
        TextView name;

        public viewHolder(@NonNull View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.item_text);
        }
    }

    public RecyclerViewAdapter(String[] n, Context ctx) {
        names = n;
        context = ctx;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layout = LayoutInflater.from(context).inflate(R.layout.item_layout, viewGroup, false);

        return new viewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder viewHolder, int i) {
        viewHolder.name.setText(names[i]);
    }

    @Override
    public int getItemCount() {
        return names.length;
    }
}
